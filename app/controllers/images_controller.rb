class ImagesController < ApplicationController

  #Esto es un callBack, "before_action" invoca a un metodo antes de cada accion
  before_action :set_image, only: [:show, :edit, :update, :destroy]

	def index
    puts '############### estoy en el index ############'
		@images = Image.all
	end

	def new
		#Nota: cuando se llama a la accion "new" 
		#este automaticamente busca la vista "new.html.erb"
		#Una vez oprimido el boton del formulario, este llama a la accion "create"

		@image = Image.new
	end

	def create
		#render plain: params[:image].inspect
		@image = Image.new image_params
		@image.save # guarda la imagen en la base de datos

		redirect_to @image 
	end

	def show
    puts '############### estoy en show ############'
  end

	def edit
		#Nota: cuando se llama a la accion "edit"
		#este automaticamente busca la vista "edit.html.erb"
		#Una vez oprimido el boton del formulario, este llama a la accion "update"
	end

	def update
		@image.update image_params
		redirect_to @image
	end

  def destroy
    puts '############### estoy en el destroy ############'
    @image.destroy
    redirect_to images_path
  end

	private
	def image_params
		params.require(:image).permit(:description)
  end

  def set_image
    #NOTA: este metodo es llamado por un callBack (before_action)
    #por lo tanto este metodo es llamado cada vez que se llaman a los metodos :show, :edit, :update, :destroy
    @image = Image.find params[:id] #busca una imagen desde la base de datos con un "id" (ingresado desde la url)
  end
end
