# README

This README would normally document whatever steps are necessary to get the
application up and running.

Esta es la primera vez que uso gitLab y a la vez estoy aprendiendo RubyOnRails. 
En este repositorio se aloja un proyecto desarrollado en RubyOnRails, denominado Railsgram.

Railsgram es el resultado del curso ofrecido por Raul Palacios:
https://www.youtube.com/watch?v=Nqwbp5djGtM&list=PLIddmSRJEJ0uaT5imV49pJqP8CGSqN-7E
Publicado el 29 oct. 2016



Things you may want to cover:

* Ruby version 2.3.3 (i386-mingw32)
* Rails version 5.1.4
* Node.js 8.9.4 (x64)
* Aunque es Desarrollado en Windows 10 (64bits), debería correr en cualquier Ubuntu y MAC
* Herramienta de desarrollo: RubyMine 2017.3.2
* System dependencies:
    Es necesario instalar NodeJS

* Configuration

* Database creation postgres pgAdmin4 v2.1

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
